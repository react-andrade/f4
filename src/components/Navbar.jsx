import React from "react";

export default function Navbar() {
    return (
        <nav>
            <a href="#">
                <img src="../../public/img/logo/airbnb.svg" alt='airbnb-logo' />
            </a>
        </nav>
    );
}