import React from "react";

export default function Hero() {
    return (
        <section className="hero-section">
            <div className="hero-img">
                <div>
                    <img src="../../public/img/hero/image 22.png" />
                </div>
                <div>
                    <img src="../../public/img/hero/image 24.png" />
                    <img src="../../public/img/hero/image 23.png" />
                </div>
                <div>
                    <img src="../../public/img/hero/image 25.png" />
                    <img src="../../public/img/hero/image 26.png" />
                </div>
                <div>
                    <img src="../../public/img/hero/image 27.png" />
                    <img src="../../public/img/hero/image 28.png" />

                </div>
                <div>
                    <img src="../../public/img/hero/image 29.png" />
                    <img src="../../public/img/hero/image 30.png" />
                </div>
            </div>
            <div className="hero-description">
                <h2>Online Experiences</h2>
                <p>Join unique interactive activities led by one-of-a-kind hosts—all without leaving home.</p>
            </div>
        </section>
    );
}