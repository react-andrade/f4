import React from "react";
import Navbar from './components/Navbar';
import Hero from './components/Hero';
import Card from './components/Card';
import './assets/style.css';

export default function App() {
    return (
        <>
            <Navbar />
            <Hero />
            <div className="cards-div-container">
                <Card
                    status='sold out'
                    img='../../public/img/card/image 12.png'
                    alt='swimmer'
                    rating={5.0}
                    rating2={6}
                    country='USA'
                    desc='Life lessons with Katie Zaferes'
                    price={136}
                />
                <Card
                    status='online'
                    img='../../public/img/card/wedding-photography.png'
                    alt='wedding'
                    rating={parseFloat(5.0)}
                    rating2={parseInt(30)}
                    country='USA'
                    desc='Learn Wedding Photography'
                    price={125}
                />
                <Card
                    img='../../public/img/card/mountain-bike.png'
                    alt='mountain-bike'
                    rating={parseFloat(4.8)}
                    rating2={parseInt(2)}
                    country='USA'
                    desc='Group Mountain Biking'
                    price={50}
                />

            </div>
        </>
    )
}